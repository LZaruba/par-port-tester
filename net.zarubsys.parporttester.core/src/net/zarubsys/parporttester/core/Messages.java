package net.zarubsys.parporttester.core;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "net.zarubsys.parporttester.core.messages"; //$NON-NLS-1$
	public static String OutputRunnable_preparingData;
	public static String OutputRunnable_processing;
	public static String OutputRunnable_running;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
