/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.core;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.NullProgressMonitor;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class Calibration {
	
	private int delayCompensation = 0;

	private static final Logger LOG = Logger.getLogger(Calibration.class.getName());
	private static final int OUTPUT_ADDRESS = Integer.parseInt(System.getProperty("calibrationOutputAddress", "888"));
	private static final int TEST_HALF_PERIOD = Integer.parseInt(System.getProperty("calibrationHalfPeriod", "50"));
	private static final int TEST_REPETITION_COUNT = Integer.parseInt(System.getProperty("calibrationRepetitionCount", "15"));
	private static final boolean SKIP_CALIBRATION = Boolean.parseBoolean(System.getProperty("skipCalibration", "false"));
	private static final int MANUAL_DELAY_COMPENSATION = Integer.parseInt(System.getProperty("delayCompensation", "0"));
	private static final Calibration INSTANCE = new Calibration();
	
	private Calibration() {
		// to prevent instantiation
	}
	
	public static Calibration getInstance() {
		return INSTANCE;
	}
	
	public int getDelayCompensation() {
		return MANUAL_DELAY_COMPENSATION + delayCompensation;
	}
	
	public int callibrate() {
		if (SKIP_CALIBRATION) return 0;
		LOG.info("Going to calibrate...");
		OutputTestResult result = new OutputTestResult();
		OutputRunnable<OutputTestResult> runnable = new OutputRunnable<OutputTestResult>(TEST_HALF_PERIOD, TEST_HALF_PERIOD, 0, 0, 
				TEST_REPETITION_COUNT, OUTPUT_ADDRESS, OUTPUT_ADDRESS, result);
		try {
			runnable.run(new NullProgressMonitor());
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error while running calibration!", e);
			throw new RuntimeException("Internal Error. Please contact administrator.");
		}
		long duration = result.getDuration();
		delayCompensation = calculateCompensationDelay(duration);
		LOG.info("Finished calibration with delay compensation " + delayCompensation + ". Manual delay compensation is " + MANUAL_DELAY_COMPENSATION + ".");
		return delayCompensation;
	}
	
	private int calculateCompensationDelay(long duration) {
		double halfPeriodDuration = (double) duration / ((double) TEST_REPETITION_COUNT * 2);
		double doubleCompensation = (double) TEST_HALF_PERIOD - halfPeriodDuration;
		int compensation = (int) Math.round(doubleCompensation);
		return compensation;
	}

}
