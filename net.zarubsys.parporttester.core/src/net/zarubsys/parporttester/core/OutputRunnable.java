/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.core;

import java.lang.reflect.InvocationTargetException;

import net.zarubsys.parporttester.io.ParallelPortAccessor;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class OutputRunnable<T extends OutputTestResult> implements IRunnableWithProgress {
	
	private final int lowDuration;
	private final int highDuration;
	private final int lowValue;
	private final int highValue;
	private final int repetitionCount;
	private final int baseAddress;
	private final int outputAddress;
	protected final T resultContainer;
	
	public OutputRunnable(int lowDuration, int highDuration, int lowValue, int highValue, int repetitionCount, 
			int baseAddress, int outputAddress, T resultContainer) {
		this.lowDuration = lowDuration;
		this.highDuration = highDuration;
		this.lowValue = lowValue;
		this.highValue = highValue;
		this.repetitionCount = repetitionCount;
		this.baseAddress = baseAddress;
		this.outputAddress = outputAddress;
		this.resultContainer = resultContainer;
	}

	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		int delayCompensation = Calibration.getInstance().getDelayCompensation();
		int compensatedHighDuration = highDuration + delayCompensation;
		int compensatedLowDuration = lowDuration + delayCompensation;
		
		monitor.beginTask(Messages.OutputRunnable_running, repetitionCount);
		monitor.subTask(Messages.OutputRunnable_preparingData);
		ParallelPortAccessor accessor = new ParallelPortAccessor();
		accessor.openLptPort(baseAddress);
		callStartHook(accessor);
		long time = System.currentTimeMillis();
		for (int i = 0; i < repetitionCount; i++) {
			if (repetitionCount <= 10 || i % 10 == 0) {
				monitor.worked(repetitionCount <= 10 ? 1 : 10);
				monitor.subTask(Messages.OutputRunnable_processing + (i + 1) + "/" + repetitionCount + "..."); //$NON-NLS-2$ //$NON-NLS-3$
			}
			
			accessor.writeOneByte(outputAddress, highValue);
			try {
				Thread.sleep(compensatedHighDuration);
			} catch (InterruptedException e) {
				// ignore
			}
			
			accessor.writeOneByte(outputAddress, lowValue);
			try {
				Thread.sleep(compensatedLowDuration);
			} catch (InterruptedException e) {
				// ignore
			}
			
			if (monitor.isCanceled()) {
				callStopHook();
				fillResult(time, i + 1, false);
				accessor.closeLptPort();
				return;
			}
			callCycleHook(i);
		}
		callStopHook();
		fillResult(time, repetitionCount, true);
		monitor.done();
		accessor.closeLptPort();
	}

	protected void fillResult(long time, int repetitionCount, boolean finishedNormally) {
		resultContainer.setDuration(System.currentTimeMillis() - time);
		resultContainer.setRepetitionsDone(repetitionCount);
		resultContainer.setFinishedNormally(finishedNormally);
	}

	protected void callStopHook() {
		// empty (check subtypes)	
	}

	protected void callStartHook(ParallelPortAccessor accessor) {
		// empty (check subtypes)
	}

	protected void callCycleHook(int i) {
		// empty (check subtypes)
	}
	
}
