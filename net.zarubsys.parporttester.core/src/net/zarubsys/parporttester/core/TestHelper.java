/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.core;

import net.zarubsys.parporttester.io.ParallelPortAccessor;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class TestHelper {
	
	public static void writeData(int baseAddress, int data, int status, int control) {
		ParallelPortAccessor accessor = new ParallelPortAccessor();
		accessor.openLptPort(baseAddress);
		accessor.writeOneByte(baseAddress, data);
		accessor.writeOneByte(baseAddress + 1, status);
		accessor.writeOneByte(baseAddress + 2, control);
		accessor.closeLptPort();
	}
	
	public static int[] readData(int baseAddress) {
		ParallelPortAccessor accessor = new ParallelPortAccessor();
		accessor.openLptPort(baseAddress);
		int[] result = new int[3];
		result[0] = accessor.readOneByte(baseAddress);
		result[1] = accessor.readOneByte(baseAddress + 1);
		result[2] = accessor.readOneByte(baseAddress + 2);
		accessor.closeLptPort();
		return result;
	}
	
	

}
