/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.core.resp;

import net.zarubsys.parporttester.core.OutputRunnable;
import net.zarubsys.parporttester.io.ParallelPortAccessor;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class OutputRunnableWithResp extends OutputRunnable<OutputTestResultWithResp> {
	
	protected final InputRunnable inputThread;
	
	public OutputRunnableWithResp(int lowDuration, int highDuration,
			int lowValue, int highValue, int repetitionCount, int baseAddress,
			int outputAddress, int inputAddress, int inputPattern, OutputTestResultWithResp resultContainer) {
		super(lowDuration, highDuration, lowValue, highValue, repetitionCount,
				baseAddress, outputAddress, resultContainer);
		this.inputThread = new InputRunnable(inputPattern, inputAddress);
	}
	
	@Override
	protected void callStartHook(ParallelPortAccessor accessor) {
		super.callStartHook(accessor);
		inputThread.setAccessor(accessor);
		Thread input = new Thread(inputThread, "InputThread");
		input.start();
	}
	
	@Override
	protected void callStopHook() {
		super.callStopHook();
		inputThread.terminate();
	}
	
	@Override
	protected void fillResult(long time, int i, boolean finishedNormally) {
		super.fillResult(time, i, finishedNormally);
		resultContainer.setChangesCount(inputThread.getChangesReceived());
		resultContainer.setTimeToFirstInput(inputThread.getTimeToFirstResponse());
	}

}
