/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.core.a1s;

import net.zarubsys.parporttester.core.resp.OutputTestResultWithResp;
import net.zarubsys.parporttester.core.resp.OutputRunnableWithResp;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class A1SRunnable extends OutputRunnableWithResp {

	public A1SRunnable(int lowValue,
			int highValue, int repetitionCount, int baseAddress,
			int outputAddress, int inputAddress, int inputPattern,
			OutputTestResultWithResp resultContainer) {
		super(1000, 1000, lowValue, highValue, repetitionCount,
				baseAddress, outputAddress, inputAddress, inputPattern, resultContainer);
	}
	
}
