/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class net_zarubsys_parporttester_io_ParallelPortAccessor */

#ifndef _Included_net_zarubsys_parporttester_io_ParallelPortAccessor
#define _Included_net_zarubsys_parporttester_io_ParallelPortAccessor
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     net_zarubsys_parporttester_io_ParallelPortAccessor
 * Method:    closePort
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_closePort
  (JNIEnv *, jclass, jint);

/*
 * Class:     net_zarubsys_parporttester_io_ParallelPortAccessor
 * Method:    openPort
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_openPort
  (JNIEnv *, jclass, jint);

/*
 * Class:     net_zarubsys_parporttester_io_ParallelPortAccessor
 * Method:    readByte
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_readByte
  (JNIEnv *, jclass, jint);

/*
 * Class:     net_zarubsys_parporttester_io_ParallelPortAccessor
 * Method:    writeByte
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_writeByte
  (JNIEnv *, jclass, jint, jint);

#ifdef __cplusplus
}
#endif
#endif
