/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <errno.h>
#include <sys/io.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/time.h>

#include "net_zarubsys_parporttester_io_ParallelPortAccessor.h"

JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_closePort (JNIEnv * env, jclass this, jint address) {
	 if (ioperm(address, 3, 0) < 0) {
                return errno;
        }
        return 0;
}

/*
 * Class:     net_zarubsys_parporttester_io_ParallelPortAccessor
 * Method:    openPort
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_openPort (JNIEnv * env, jclass this, jint address) {
	if (ioperm(address, 3, 1) < 0) {
                return errno;
        }
        return 0;
}

/*
 * Class:     net_zarubsys_parporttester_io_ParallelPortAccessor
 * Method:    readByte
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_readByte (JNIEnv * env, jclass this, jint address) {
	return inb(address);
}

/*
 * Class:     net_zarubsys_parporttester_io_ParallelPortAccessor
 * Method:    writeByte
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_net_zarubsys_parporttester_io_ParallelPortAccessor_writeByte (JNIEnv * env, jclass this, jint address, jint value) {
	outb(value, address);
	return 0;
}

