\select@language {czech}
\contentsline {section}{\numberline {1}Specifikace \IeC {\'u}lohy}{3}
\contentsline {subsection}{\numberline {1.1}C\IeC {\'\i }le a pou\IeC {\v z}it\IeC {\'\i } aplikace}{3}
\contentsline {subsection}{\numberline {1.2}Funk\IeC {\v c}n\IeC {\'\i } po\IeC {\v z}adavky}{3}
\contentsline {subsection}{\numberline {1.3}Po\IeC {\v z}adavky na u\IeC {\v z}ivatelsk\IeC {\'e} rozhran\IeC {\'\i }}{4}
\contentsline {section}{\numberline {2}Anal\IeC {\'y}za zad\IeC {\'a}n\IeC {\'\i }}{4}
\contentsline {section}{\numberline {3}Anal\IeC {\'y}za u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{5}
\contentsline {section}{\numberline {4}Pou\IeC {\v z}it\IeC {\'e} technologie}{8}
\contentsline {subsection}{\numberline {4.1}Java}{8}
\contentsline {subsection}{\numberline {4.2}Eclipse}{8}
\contentsline {subsubsection}{\numberline {4.2.1}Eclipse Platform}{8}
\contentsline {paragraph}{OSGi}{10}
\contentsline {subparagraph}{OSGi bundle}{10}
\contentsline {subparagraph}{Equinox}{11}
\contentsline {paragraph}{Eclipse Plugin}{11}
\contentsline {subparagraph}{Na\IeC {\v c}ten\IeC {\'\i } pluginu}{11}
\contentsline {subparagraph}{Obsah pluginu}{12}
\contentsline {paragraph}{Eclipse Fragment}{12}
\contentsline {paragraph}{Eclipse Feature}{12}
\contentsline {paragraph}{Eclipse Extensions}{12}
\contentsline {subsubsection}{\numberline {4.2.2}Eclipse RCP}{13}
\contentsline {paragraph}{Pracovn\IeC {\'\i } prost\IeC {\v r}ed\IeC {\'\i }, perspektivy, pohledy, editory}{13}
\contentsline {paragraph}{SWT}{13}
\contentsline {paragraph}{JFace}{14}
\contentsline {subsubsection}{\numberline {4.2.3}Eclipse IDE}{14}
\contentsline {section}{\numberline {5}Implementace}{15}
\contentsline {subsection}{\numberline {5.1}Implementace prvk\IeC {\r u} u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{15}
\contentsline {section}{\numberline {6}Testy}{16}
\contentsline {section}{\numberline {7}Z\IeC {\'a}v\IeC {\v e}r}{17}
