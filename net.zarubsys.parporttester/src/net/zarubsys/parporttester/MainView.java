/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester;

import java.lang.reflect.Constructor;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.zarubsys.parporttester.a1s.A1SWizard;
import net.zarubsys.parporttester.dynNOResp.DynNoRespWizard;
import net.zarubsys.parporttester.dynResp.DynWizard;
import net.zarubsys.parporttester.test.TestWizard;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class MainView extends ViewPart {
	
	private static final Logger LOG = Logger.getLogger(ViewPart.class.getName());

	public MainView() {
		// ignore
	}

	@Override
	public void createPartControl(final Composite parent) {
		GridData gd;
		Label label;
		
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout gl = new GridLayout(1, false);
		gl.verticalSpacing = 20;
        composite.setLayout(gl);
        
        label = new Label(composite, SWT.NONE);
        gd = new GridData(SWT.FILL, SWT.CENTER, false, false);
        gd.widthHint = 220;
        label.setLayoutData(gd);
        label.setText(Messages.MainView_chooseAnAction);
        
        Button dynProcWithResp = new Button(composite, SWT.PUSH);
        gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gd.widthHint = 220;
        dynProcWithResp.setLayoutData(gd);
        dynProcWithResp.setText(Messages.MainView_actions_dynWithResp);
        dynProcWithResp.addSelectionListener(new RunWizardListener(DynWizard.class));
        
        Button a1sProcWithResp = new Button(composite, SWT.PUSH);
        gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gd.widthHint = 220;
        a1sProcWithResp.setLayoutData(gd);
        a1sProcWithResp.setText(Messages.MainView_actions_1sWithResp);
        a1sProcWithResp.addSelectionListener(new RunWizardListener(A1SWizard.class));
        
        Button dynProcNOResp = new Button(composite, SWT.PUSH);
        gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gd.widthHint = 220;
        dynProcNOResp.setLayoutData(gd);
        dynProcNOResp.setText(Messages.MainView_actions_dynNoResp);
        dynProcNOResp.addSelectionListener(new RunWizardListener(DynNoRespWizard.class)); 
        
        Button test = new Button(composite, SWT.PUSH);
        gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gd.widthHint = 220;
        test.setLayoutData(gd);
        test.setText(Messages.MainView_actions_test);
        test.addSelectionListener(new RunWizardListener(TestWizard.class)); 
        
        // to create space between exit button and other elements
        new Composite(composite, SWT.NONE);
        
        Button exit = new Button(composite, SWT.PUSH);
        gd = new GridData(SWT.RIGHT, SWT.CENTER, false, false);
        gd.widthHint = 120;
        exit.setLayoutData(gd);
        exit.setText(Messages.MainView_actions_EXIT);
		exit.setImage(ParPortTesterPlugin.getDefault().getImageRegistry().get(ParPortTesterPlugin.EXIT));
        exit.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean doExit = MessageDialog.openConfirm(PlatformUI.getWorkbench().getDisplay().getActiveShell(), Messages.MainView_exit_title, Messages.MainView_exit_message);
				if (doExit) {
					PlatformUI.getWorkbench().close();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// ignore
			}
		});
        
        label = new Label(composite, SWT.NONE);
        gd = new GridData(SWT.CENTER, SWT.CENTER, true, false);
        label.setLayoutData(gd);
        label.setText("(c) Lukas Zaruba 2011");
	}


	@Override
	public void setFocus() {
		// ignore
	}
	
	private static class RunWizardListener implements SelectionListener {

		private final Class<? extends Wizard> wizardClass;

		public RunWizardListener(Class<? extends Wizard> wizardClass) {
			this.wizardClass = wizardClass;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			Wizard wizard;
			try {
				Constructor<? extends Wizard> constructor = wizardClass.getConstructor();
				wizard = constructor.newInstance();
			} catch (Exception ex) {
				LOG.log(Level.SEVERE, "Cannot instantiate wizard!", ex); //$NON-NLS-1$
				throw new RuntimeException(Messages.MainView_error_internal);
			}
			WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().
					getActiveWorkbenchWindow().getShell(), wizard);
			dialog.create();
			dialog.open();
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// ignored
		}
		
	}

}
