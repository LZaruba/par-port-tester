package net.zarubsys.parporttester.test;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "net.zarubsys.parporttester.test.messages"; //$NON-NLS-1$
	public static String TestPage_actions_readValues;
	public static String TestPage_actions_setValues;
	public static String TestPage_description;
	public static String TestPage_errors_invalidOctalString;
	public static String TestPage_labels_control;
	public static String TestPage_labels_data;
	public static String TestPage_labels_input;
	public static String TestPage_labels_output;
	public static String TestPage_labels_port;
	public static String TestPage_labels_status;
	public static String TestPage_title;
	public static String TestWizard_title;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
