/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.test;

import net.zarubsys.parporttester.core.TestHelper;
import net.zarubsys.parporttester.utils.IOHWUtils;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class TestPage extends WizardPage {
	
	private Combo addressCombo;
	private Text inputData;
	private Text inputStatus;
	private Text inputControl;
	
	private Text outputData;
	private Text outputStatus;
	private Text outputControl;

	public TestPage() {
		super("Test"); //$NON-NLS-1$
		setTitle(Messages.TestPage_title);
		setDescription(Messages.TestPage_description);
		setPageComplete(false);
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		createControlInternal(composite);
		setControl(composite);
		validate();
	}

	private void validate() {
		if (!validateOutput(outputData.getText(), Messages.TestPage_labels_data)) return;
		if (!validateOutput(outputStatus.getText(), Messages.TestPage_labels_status)) return;
		if (!validateOutput(outputControl.getText(), Messages.TestPage_labels_control)) return;
	}

	private void createControlInternal(Composite composite) {
		Label label;
		label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.TestPage_labels_port);
		addressCombo = new Combo(composite, SWT.NONE);
		addressCombo.setItems(IOHWUtils.getInstance().getPortNames());
		addressCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		addressCombo.select(0);
		createOutput(composite);
		createInput(composite);
	}

	private void createInput(Composite composite) {
		Group group = new Group(composite, SWT.SHADOW_OUT);
		group.setLayout(new GridLayout(2, false));
		group.setText(Messages.TestPage_labels_input);
		
		Label label;
		label = new Label(group, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.TestPage_labels_data);
		inputData = createOutputText(group, "??????????"); //$NON-NLS-1$
		
		label = new Label(group, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.TestPage_labels_status);
		inputStatus = createOutputText(group, "??????????"); //$NON-NLS-1$
		
		label = new Label(group, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.TestPage_labels_control);
		inputControl = createOutputText(group, "??????????"); //$NON-NLS-1$
		
		Button setButton = new Button(group, SWT.NONE);
		setButton.setText(Messages.TestPage_actions_readValues);
		setButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		setButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				int[] result = TestHelper.readData(IOHWUtils.getInstance().getPortAddress(addressCombo.getSelectionIndex()));
				String data = normalize(Integer.toBinaryString(result[0]));
				String status = normalize(Integer.toBinaryString(result[1]));
				String control = normalize(Integer.toBinaryString(result[2]));
				
				inputData.setText(data);
				inputStatus.setText(status);
				inputControl.setText(control);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// ignore
			}
		});
	}
	
	private String normalize(String text) {
		int count = 8 - text.length();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++) {
			sb.append(0);
		}
		sb.append(text);
		return sb.toString();
	}

	private void createOutput(Composite composite) {
		Group group = new Group(composite, SWT.SHADOW_OUT);
		group.setLayout(new GridLayout(2, false));
		group.setText(Messages.TestPage_labels_output);
		
		Label label;
		label = new Label(group, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.TestPage_labels_data);
        outputData = createInputText(group, "00000000"); //$NON-NLS-1$
		
		label = new Label(group, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.TestPage_labels_status);
        outputStatus = createInputText(group, "00000000"); //$NON-NLS-1$
		
		label = new Label(group, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.TestPage_labels_control);
        outputControl = createInputText(group, "00000000"); //$NON-NLS-1$
		
		Button setButton = new Button(group, SWT.NONE);
		setButton.setText(Messages.TestPage_actions_setValues);
		setButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		setButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				int baseAddress = IOHWUtils.getInstance().getPortAddress(addressCombo.getSelectionIndex());
				int data = Integer.parseInt(outputData.getText(), 2);
				int status = Integer.parseInt(outputStatus.getText(), 2);
				int control = Integer.parseInt(outputControl.getText(), 2);
				TestHelper.writeData(baseAddress, data, status, control);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// ignore
			}
		});
	}
	
	private Text createInputText(Composite parent, String defaultValue) {
		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		text.setText(defaultValue);
		text.setTextLimit(8);
		text.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				validate();
			}
		});
		
		return text;
	}
	
	private Text createOutputText(Composite parent, String defaultValue) {
		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
		text.setText(defaultValue);
		return text;
	}
	
	private void setUserOutput(String text) {
        if (text != null) {
                setErrorMessage(text);
        } else {
                setErrorMessage(null);
        }
	}
	
	private boolean validateOutput(String text, String namePrefix) {
		String errorMessage = namePrefix + Messages.TestPage_errors_invalidOctalString;
		if (text == null || text.length() != 8) {
			setUserOutput(errorMessage);
			return false;
		}
		
		try {
			Integer.parseInt(text, 2);
		} catch (Exception e) {
			setUserOutput(errorMessage);
			return false;
		}
		
		setUserOutput(null); // clear errors
		return true;
	}

}
