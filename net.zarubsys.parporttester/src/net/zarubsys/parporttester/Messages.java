package net.zarubsys.parporttester;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "net.zarubsys.parporttester.messages"; //$NON-NLS-1$
	public static String MainView_actions_1sWithResp;
	public static String MainView_actions_dynNoResp;
	public static String MainView_actions_dynWithResp;
	public static String MainView_actions_EXIT;
	public static String MainView_actions_test;
	public static String MainView_error_internal;
	public static String MainView_exit_message;
	public static String MainView_exit_title;
	public static String MainView_chooseAnAction;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
