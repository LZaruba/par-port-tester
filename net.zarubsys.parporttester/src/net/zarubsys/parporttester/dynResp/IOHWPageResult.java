/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.dynResp;

import net.zarubsys.parporttester.dynNOResp.OHWPageResult;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class IOHWPageResult extends OHWPageResult {

	private final int inputAddress;
	private final int inputMask;

	public IOHWPageResult(OHWPageResult ohwPageResult, int inputAddress, int inputMask) {
		super(ohwPageResult.getBaseAddress(), ohwPageResult.getOutputAddress(),  ohwPageResult.getOutputMask());
		this.inputAddress = inputAddress;
		this.inputMask = inputMask;
	}

	public int getInputAddress() {
		return inputAddress;
	}

	public int getInputMask() {
		return inputMask;
	}
	
}
