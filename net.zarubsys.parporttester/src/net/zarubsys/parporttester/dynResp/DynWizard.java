/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.dynResp;

import net.zarubsys.parporttester.core.resp.OutputTestResultWithResp;
import net.zarubsys.parporttester.core.resp.OutputRunnableWithResp;

import org.eclipse.jface.operation.IRunnableWithProgress;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class DynWizard extends AbstractRespWizard {
	
	private DynParamsPage paramsPage = new DynParamsPage();
	private IOHWPage ioHWPage = new IOHWPage();

	public DynWizard() {
		super();
		setWindowTitle(Messages.DynWizard_title);
		setNeedsProgressMonitor(true);
	}
	
	@Override
	public void addPages() {
		addPage(ioHWPage);
		addPage(paramsPage);
	}
	
	protected IRunnableWithProgress prepareRunnable(OutputTestResultWithResp result) {
		IOHWPageResult iohwResult = ioHWPage.getResultData();
		DynParamsResult paramsResult = paramsPage.getResultData();
		OutputRunnableWithResp outputThread = new OutputRunnableWithResp(paramsResult.getLowDuration(), paramsResult.getHighDuration(), 
				0, iohwResult.getOutputMask(), paramsResult.getRepetitionCount(), iohwResult.getBaseAddress(), 
				iohwResult.getOutputAddress(), iohwResult.getInputAddress(), iohwResult.getInputMask(), result);
		return outputThread;
	}
	
}
