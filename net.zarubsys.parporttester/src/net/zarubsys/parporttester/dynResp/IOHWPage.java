/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.dynResp;

import net.zarubsys.parporttester.dynNOResp.OHWPage;
import net.zarubsys.parporttester.dynNOResp.OHWPageResult;
import net.zarubsys.parporttester.utils.IOHWUtils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class IOHWPage extends OHWPage {

	private Combo inputOffset;
	private Text inputMask;
	
	@Override
	protected void createControlInternal(Composite composite) {
		Label label;
		super.createControlInternal(composite);
		label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.IOHWPage_labels_inputOffset);
		inputOffset = createOffsetCombo(composite);
		inputOffset.select(1);
		
		label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.IOHWPage_labels_inputMask);
		inputMask = createMaskText(composite, "00100000"); //$NON-NLS-1$
	}
	
	@Override
	protected boolean validate() {
		if (!super.validate()) return false;
		if (!validateOffset(inputMask.getText(), Messages.IOHWPage_labels_input)) return false;
		return true;
	}
	
	@Override
	public IOHWPageResult getResultData() {
		OHWPageResult ohwPageResult = super.getResultData();
		int inputAddress = IOHWUtils.getInstance().addOffset(ohwPageResult.getBaseAddress(), inputOffset.getSelectionIndex());
		int inputMaskValue = Integer.parseInt(inputMask.getText(), 2);
		return new IOHWPageResult(ohwPageResult, inputAddress, inputMaskValue);
	}

}
