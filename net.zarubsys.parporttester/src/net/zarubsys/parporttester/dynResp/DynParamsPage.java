/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.dynResp;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class DynParamsPage extends WizardPage {

	private Spinner lowDuration;
	private Spinner highDuration;
	private Spinner repetitionCount;

	public DynParamsPage() {
		super("Params"); //$NON-NLS-1$
		setTitle(Messages.DynParamsPage_title);
		setDescription(Messages.DynParamsPage_description);
	}

	@Override
	public void createControl(Composite parent) {
		Label label;
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		
		label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.DynParamsPage_labels_lowDuration);
        lowDuration = createSpinner(composite, 10, 500, 60);
		
		label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.DynParamsPage_labels_highDuration);
		highDuration = createSpinner(composite, 10, 500, 120);
		
		label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
        label.setText(Messages.DynParamsPage_lables_repetitionCount);
		repetitionCount = createSpinner(composite, 1, 10000, 100);
		
		validate();
		
		setControl(composite);

	}
	
	private void setUserOutput(String text) {
        if (text != null) {
                setErrorMessage(text);
                setPageComplete(false);
        } else {
                setErrorMessage(null);
                setPageComplete(true);
        }
	}

	private void validate() {
		if (!checkSpinner(lowDuration, Messages.DynParamsPage_labels_error_lowDuration)) return;
		if (!checkSpinner(highDuration, Messages.DynParamsPage_labels_error_highDuration)) return;
		if (!checkSpinner(repetitionCount, Messages.DynParamsPage_lables_repetitionCount)) return;
	}

	private boolean checkSpinner(Spinner spinner, String errorPrefix) {
		int value = Integer.parseInt(spinner.getText());
		int max = spinner.getMaximum();
		int min = spinner.getMinimum();
		if (value > max || value < min) {
			setUserOutput(errorPrefix + Messages.DynParamsPage_errors_valueMustBeInbounds + min + ", " + max + ")!"); //$NON-NLS-2$ //$NON-NLS-3$
			return false;
		}
		setUserOutput(null);
		return true;
	}
	
	private Spinner createSpinner(Composite parent, int min, int max, int selection) {
		Spinner spinner = new Spinner(parent, SWT.BORDER);
		spinner.setMinimum(min);
		spinner.setMaximum(max);
		spinner.setSelection(selection);
		spinner.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				validate();
				
			}
		});
		
		return spinner;
	}
	
	public DynParamsResult getResultData() {
		int lowDurationValue = Integer.parseInt(lowDuration.getText());
		int highDurationValue = Integer.parseInt(highDuration.getText());
		int repetitionCountValue = Integer.parseInt(repetitionCount.getText());
		return new DynParamsResult(lowDurationValue, highDurationValue, repetitionCountValue);
	}

}
