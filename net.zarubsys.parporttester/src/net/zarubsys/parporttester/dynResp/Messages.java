package net.zarubsys.parporttester.dynResp;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "net.zarubsys.parporttester.dynResp.messages"; //$NON-NLS-1$
	public static String AbstractRespWizard_result_error;
	public static String AbstractRespWizard_result_ok;
	public static String AbstractRespWizard_result_title_error;
	public static String AbstractRespWizard_result_title_ok;
	public static String DynParamsPage_description;
	public static String DynParamsPage_errors_valueMustBeInbounds;
	public static String DynParamsPage_labels_error_highDuration;
	public static String DynParamsPage_labels_error_lowDuration;
	public static String DynParamsPage_labels_highDuration;
	public static String DynParamsPage_labels_lowDuration;
	public static String DynParamsPage_lables_repetitionCount;
	public static String DynParamsPage_title;
	public static String DynWizard_title;
	public static String IOHWPage_labels_input;
	public static String IOHWPage_labels_inputMask;
	public static String IOHWPage_labels_inputOffset;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
