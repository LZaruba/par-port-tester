package net.zarubsys.parporttester.dynNOResp;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "net.zarubsys.parporttester.dynNOResp.messages"; //$NON-NLS-1$
	public static String DynNoRespWizard_result_error;
	public static String DynNoRespWizard_result_ok;
	public static String DynNoRespWizard_result_title_error;
	public static String DynNoRespWizard_result_title_ok;
	public static String DynNoRespWizard_title;
	public static String OHWPage_description;
	public static String OHWPage_errors_illegalOffset;
	public static String OHWPage_labels_output;
	public static String OHWPage_labels_outputMask;
	public static String OHWPage_labels_outputOffset;
	public static String OHWPage_labels_port;
	public static String OHWPage_title;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
