/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.dynNOResp;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class OHWPageResult {
	
	private final int baseAddress;
	private final int outputAddress;
	private final int outputMask;
	
	public OHWPageResult(int baseAddress, int outputAddress, int outputMask) {
		this.baseAddress = baseAddress;
		this.outputAddress = outputAddress;
		this.outputMask = outputMask;
	}

	public int getBaseAddress() {
		return baseAddress;
	}

	public int getOutputAddress() {
		return outputAddress;
	}

	public int getOutputMask() {
		return outputMask;
	}

}
