/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.dynNOResp;

import java.lang.reflect.InvocationTargetException;

import net.zarubsys.parporttester.core.OutputTestResult;
import net.zarubsys.parporttester.core.OutputRunnable;
import net.zarubsys.parporttester.dynResp.DynParamsPage;
import net.zarubsys.parporttester.dynResp.DynParamsResult;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class DynNoRespWizard extends Wizard {
	
	private static final String ERROR_PATTERN = Messages.DynNoRespWizard_result_error;
	private static final String OK_PATTERN = Messages.DynNoRespWizard_result_ok;

	private OHWPage ohwPage = new OHWPage();
	private DynParamsPage paramsPage = new DynParamsPage();

	public DynNoRespWizard() {
		super();
		setWindowTitle(Messages.DynNoRespWizard_title);
		setNeedsProgressMonitor(true);
	}
	
	@Override
	public void addPages() {
		addPage(ohwPage);
		addPage(paramsPage);
	}
	
	@Override
	public boolean performFinish() {
		OutputTestResult result = new OutputTestResult();
		IRunnableWithProgress runnable = prepareRunnable(result);
	    try {
	            getContainer().run(true, true, runnable);
	    } catch (InterruptedException e) {
	            return false;
	    } catch (InvocationTargetException e) {
	            Throwable realException = e.getTargetException();
	            MessageDialog.openError(getShell(), Messages.DynNoRespWizard_result_title_error, realException.getMessage());
	            return false;
	    }
	    
	    String pattern = result.isFinishedNormally() ? OK_PATTERN : ERROR_PATTERN;
	    String message = String.format(pattern, result.getRepetitionsDone(), result.getDuration());
	    int kind = result.isFinishedNormally() ? MessageDialog.INFORMATION : MessageDialog.WARNING;
		MessageDialog.open(kind, getShell(), Messages.DynNoRespWizard_result_title_ok, message, SWT.NONE);
	    return true;
	}

	private IRunnableWithProgress prepareRunnable(OutputTestResult resultContainer) {
		OHWPageResult ohwResult = ohwPage.getResultData();
		DynParamsResult paramsResult = paramsPage.getResultData();
		OutputRunnable<OutputTestResult> outputThread = new OutputRunnable<OutputTestResult>(paramsResult.getLowDuration(), paramsResult.getHighDuration(), 
				0, ohwResult.getOutputMask(), paramsResult.getRepetitionCount(), ohwResult.getBaseAddress(), 
				ohwResult.getOutputAddress(), resultContainer);
		return outputThread;
	}

}
