/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester.a1s;

import net.zarubsys.parporttester.core.a1s.A1SRunnable;
import net.zarubsys.parporttester.core.resp.OutputTestResultWithResp;
import net.zarubsys.parporttester.dynResp.AbstractRespWizard;
import net.zarubsys.parporttester.dynResp.IOHWPage;
import net.zarubsys.parporttester.dynResp.IOHWPageResult;

import org.eclipse.jface.operation.IRunnableWithProgress;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class A1SWizard extends AbstractRespWizard {
	
	private final IOHWPage ioHWpage = new IOHWPage();
	private final A1SParamsPage paramsPage = new A1SParamsPage();

	public A1SWizard() {
		super();
		setWindowTitle(Messages.A1SWizard_title);
		setNeedsProgressMonitor(true);
	}
	
	@Override
	public void addPages() {
		addPage(ioHWpage);
		addPage(paramsPage);
	}

	protected IRunnableWithProgress prepareRunnable(OutputTestResultWithResp result) {
		IOHWPageResult iohwResult = ioHWpage.getResultData();
		A1SParamsPageResult paramsResult = paramsPage.getResultData();
		A1SRunnable runnable = new A1SRunnable(0, iohwResult.getOutputMask(), paramsResult.getRepetitionCount(), iohwResult.getBaseAddress(), 
				iohwResult.getOutputAddress(), iohwResult.getInputAddress(), iohwResult.getInputMask(), result);
		return runnable;
	}

}
