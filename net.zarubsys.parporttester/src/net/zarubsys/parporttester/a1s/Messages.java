package net.zarubsys.parporttester.a1s;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "net.zarubsys.parporttester.a1s.messages"; //$NON-NLS-1$
	public static String A1SParamsPage_description;
	public static String A1SParamsPage_errors_valueMustBeInBounds;
	public static String A1SParamsPage_labels_repetitionCount;
	public static String A1SParamsPage_title;
	public static String A1SWizard_title;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
