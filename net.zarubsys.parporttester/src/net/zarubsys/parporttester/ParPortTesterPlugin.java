/**
 *       Copyright (c) 2011 Lukas Zaruba
 * 
 *   This file is part of ParPortTester
 *
 *   ParPortTester is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ParPortTester is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ParPortTester. If not, see <http://www.gnu.org/licenses/>.
 */


package net.zarubsys.parporttester;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * @author  &lt;A HREF=&quot;mailto:lukas.zaruba@gmail.com&quot;&gt;Lukas Zaruba&lt;/A&gt; zarubsys.net
 */
public class ParPortTesterPlugin extends AbstractUIPlugin {

	public static final String EXIT = "exit"; //$NON-NLS-1$
	public static final String PLUGIN_ID = "net.zarubsys.parporttester"; //$NON-NLS-1$

	private static ParPortTesterPlugin INSTANCE;
	
	public ParPortTesterPlugin() {
		
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		INSTANCE = this;
	}

	public void stop(BundleContext context) throws Exception {
		INSTANCE = null;
		super.stop(context);
	}

	public static ParPortTesterPlugin getDefault() {
		return INSTANCE;
	}
	
	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);
		reg.put(EXIT, imageDescriptorFromPlugin(PLUGIN_ID, "icons/exit.png")); //$NON-NLS-1$
	}

}
